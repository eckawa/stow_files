#!/bin/bash

echo "Installing packages..."

os=`grep -E "^ID" /etc/*release | cut -d "=" -f 2`
if [ $os = "arch" ]
then
  CNT="[\e[1;36mNOTE\e[0m]"

  echo "On est sur arch. Installing packages"
  paru -S wget unzip bat ripgrep neovim zsh zoxide eza stow fzf \
  duf hyprland kitty waybar tokei btop \
  swaylock-effects wofi wlogout mako xdg-desktop-portal-hyprland-git \
  swappy grim slurp thunar polkit-gnome python-requests pamixer \
  pavucontrol brightnessctl bluez bluez-utils blueman \
  network-manager-applet gvfs thunar-archive-plugin file-roller  \
  pacman-contrib starship ttf-jetbrains-mono-nerd noto-fonts-emoji \
  lxappearance xfce4-settings fd clipman
  sleep 2

  # Start the bluetooth service
  echo -e "$CNT - Starting the Bluetooth Service..."
  sudo systemctl enable --now bluetooth.service
  sleep 2

  # Clean out other portals
  echo -e "$CNT - Cleaning out conflicting xdg portals..."
  paru -R --noconfirm xdg-desktop-portal-gnome xdg-desktop-portal-gtk
  sleep 2

  # Set some files as exacutable
  echo -e "$CNT - Setting some file as executable."
  chmod +x ~/.config/hypr/scripts/bgaction
  chmod +x ~/.config/hypr/scripts/xdg-portal-hyprland
  chmod +x ~/.config/waybar/scripts/waybar-wttr.py

  #setup the theme of gtk apps
  xfconf-query -c xsettings -p /Net/ThemeName -s "dracula"
  xfconf-query -c xsettings -p /Net/IconThemeName -s "dracula"
  gsettings set org.gnome.desktop.interface gtk-theme "dracula"
  gsettings set org.gnome.desktop.interface icon-theme "dracula"

elif [ $os = "fedora" ]
then
  echo "On est sur fedora"
  sudo dnf install bat wget unzip ripgrep neovim zsh gcc-c++ zoxide exa stow fzf
else
  echo "Dunno"
fi

echo "Installing fnm"
curl -fsSL https://fnm.vercel.app/install | bash

curl -o ~/Pictures/terafox.jpeg https://user-images.githubusercontent.com/2746374/179428810-69b440d1-7107-49f2-ab2b-aaab1c449cce.jpeg

echo "Cloning zsh plugins."
echo "... zsh-autosuggestions"
git clone https://github.com/zsh-users/zsh-autosuggestions ~/.zsh/zsh-autosuggestions
echo "... zsh-syntax-highlighting"
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ~/.zsh/zsh-syntax-highlighting
echo "... powerlevel10k"
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ~/.zsh/powerlevel10k

echo "[-] Downloading fonts [-]"
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/DroidSansMono.zip
wget https://download.jetbrains.com/fonts/JetBrainsMono-2.304.zip
unzip DroidSansMono.zip -d ~/.local/share/fonts/
unzip JetBrainsMono-2.304.zip -d ~/.local/share/fonts/
rm -rf DroidSansMono.zip JetBrainsMono-2.304.zip
fc-cache -fv
echo "done!"
