-- configs for everforest
-- vim.g.everforest_background = 'soft'
-- vim.g.everforest_diagnostic_text_highlight = 1
-- vim.g.everforest_current_word = 'underline'
-- vim.o.background = 'light'
-- local colorscheme = "everforest"

-- local colorscheme = "github_dark_high_contrast"
local colorscheme = "kanagawa"

vim.cmd.colorscheme(colorscheme)

-- Transparence
-- vim.api.nvim_set_hl(0, "Normal", { bg = "none"})
-- vim.api.nvim_set_hl(0, "NormalFloat", { bg = "none"})

-- local status_ok, _ = pcall(vim.cmd, "colorscheme " .. colorscheme)
-- if not status_ok then
  -- vim.notify("colorscheme " .. colorscheme .. " not found!")
--   return
-- end

-- require("catppuccin").setup({
--   transparent_background = true,
--   term_colors = false,
--   compile = {
--     enabled = false,
--     path = vim.fn.stdpath("cache") .. "/catppuccin",
--   },
--   dim_inactive = {
--     enabled = false,
--     shade = "dark",
--     percentage = 0.15,
--   },
--   styles = {
--     comments = { "italic" },
--     conditionals = { "italic" },
--     loops = {},
--     functions = {},
--     keywords = {},
--     strings = {},
--     variables = {},
--     numbers = {},
--     booleans = {},
--     properties = {},
--     types = {},
--     operators = {},
--   },
--   integrations = {
--     treesitter = true,
--     cmp = true,
--     gitsigns = true,
--     telescope = true,
--     nvimtree = true,
--     -- For more plugins integrations please scroll down (https://github.com/catppuccin/nvim#integrations)
--   },
--   color_overrides = {},
--   highlight_overrides = {},
-- })
