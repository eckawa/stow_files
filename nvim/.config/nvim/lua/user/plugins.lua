local fn = vim.fn

-- Automatically install packer
local install_path = fn.stdpath("data") .. "/site/pack/packer/start/packer.nvim"
if fn.empty(fn.glob(install_path)) > 0 then
	PACKER_BOOTSTRAP = fn.system({
		"git",
		"clone",
		"--depth",
		"1",
		"https://github.com/wbthomason/packer.nvim",
		install_path,
	})
	print("Installing packer close and reopen Neovim...")
	vim.cmd([[packadd packer.nvim]])
end

-- Autocommand that reloads neovim whenever you save the plugins.lua file
vim.cmd([[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerSync
  augroup end
]])

-- Use a protected call so we don't error out on first use
local status_ok, packer = pcall(require, "packer")
if not status_ok then
	return
end

-- Have packer use a popup window
packer.init({
	display = {
		open_fn = function()
			return require("packer.util").float({ border = "rounded" })
		end,
	},
})

-- Install your plugins here
return packer.startup(function(use)
	-- My plugins here

	use({ "wbthomason/packer.nvim" }) -- Have packer manage itself
	use({ "nvim-lua/plenary.nvim" }) -- Useful lua functions used by lots of plugins (telescope, gitsigns,...)
	use({ "windwp/nvim-autopairs" }) -- Autopairs, integrates with both cmp and treesitter
	use({ "numToStr/Comment.nvim" })
	use({ "kyazdani42/nvim-web-devicons" })
	use({ "kyazdani42/nvim-tree.lua" })
	use({ "akinsho/bufferline.nvim" })
	-- use({ "moll/vim-bbye" })  -- better Bdelete and Bwipeout
  use({ "ludovicchabant/vim-gutentags" }) -- Automatic tags management
	use({ "nvim-lualine/lualine.nvim" })
	use({ "akinsho/toggleterm.nvim" })
  use({ "folke/todo-comments.nvim", config = function() require("todo-comments").setup {} end })
	use({ "ahmedkhalf/project.nvim",
        config = function() require("project_nvim").setup {
          -- your configuration comes here
          -- or leave it empty to use the default settings
          -- refer to the configuration section below
        } end })
	use({ "lewis6991/impatient.nvim" }) -- improve start-up time
	use({ "lukas-reineke/indent-blankline.nvim" })
	-- use("folke/which-key.nvim")

  -- Web
  	use({ "JoosepAlviste/nvim-ts-context-commentstring" }) -- use the right type of comment depending of the language under the cursor and not the filetype (good for Vue for example)
    use({ "windwp/nvim-ts-autotag" })

	-- Colorschemes
	use({ "folke/tokyonight.nvim" })
	use({ "rebelot/kanagawa.nvim" })
  use({ 'rose-pine/neovim', as = 'rose-pine' })
  use ({ "sainnhe/everforest" })
  use ({ 'shaunsingh/nord.nvim' })
  use {'EdenEast/nightfox.nvim'}
  use ({ 'projekt0n/github-nvim-theme' })


  -- show colors in css
  use ("norcalli/nvim-colorizer.lua")

  -- TODO: a remove a l'avenir. On passe a l'utilisation de lsp-zero
	-- cmp plugins
	-- use({ "hrsh7th/nvim-cmp" }) -- The completion plugin
	-- use({ "hrsh7th/cmp-buffer" }) -- buffer completions
	-- use({ "hrsh7th/cmp-path" }) -- path completions
	-- use({ "saadparwaiz1/cmp_luasnip" }) -- snippet completions
	-- use({ "hrsh7th/cmp-nvim-lsp" })
	-- use({ "hrsh7th/cmp-nvim-lua" })

	-- snippets
	-- use({ "L3MON4D3/LuaSnip" }) --snippet engine
	-- use({ "rafamadriz/friendly-snippets" }) -- a bunch of snippets to use

	-- LSP
	-- use({ "neovim/nvim-lspconfig" }) -- enable LSP
	-- use({ "williamboman/nvim-lsp-installer" }) -- simple to use language server installer
  --

  use {
    'VonHeikemen/lsp-zero.nvim',
    requires = {
      -- LSP Support
      {'neovim/nvim-lspconfig'},
      {'williamboman/mason.nvim'},
      {'williamboman/mason-lspconfig.nvim'},

      -- Autocompletion
      {'hrsh7th/nvim-cmp'},
      {'hrsh7th/cmp-buffer'},
      {'hrsh7th/cmp-path'},
      {'saadparwaiz1/cmp_luasnip'},
      {'hrsh7th/cmp-nvim-lsp'},
      {'hrsh7th/cmp-nvim-lua'},

      -- Snippets
      {'L3MON4D3/LuaSnip'},
      {'rafamadriz/friendly-snippets'},
    }
  }

  use({ "eandrju/cellular-automaton.nvim"})

	use({ "jose-elias-alvarez/null-ls.nvim" }) -- for formatters and linters
  use ({ "RRethy/vim-illuminate" })  -- highlight current word under the cursor

	-- Telescope
	use({ "nvim-telescope/telescope.nvim" })
  use { 'nvim-telescope/telescope-fzf-native.nvim', run = 'make' }

	-- Treesitter
	use({
		"nvim-treesitter/nvim-treesitter",
    run = ":TSUpdate",
	})

  -- Copilot
  use({ "github/copilot.vim"})

  -- Pour voir l'AST genere par treesitter dans un buffer.
  use({"nvim-treesitter/playground"})

  -- montre l'arbre d'undo de vim (permet dans certain cas de plus facilement remonter dans des branches paralleles)
  use({"mbbill/undotree"})

	-- Git
	use({ "lewis6991/gitsigns.nvim" })

  -- DAP
  -- use({ "mfussenegger/nvim-dap" })
  -- use({ "rcarriga/nvim-dap-ui" })
  -- use({ "Pocco81/DAPinstall.nvim", commit="36791320a09a719834d5920ea2eef2155d75a3a0" })
  --use({ "Pocco81/dap-buddy.nvim" })

  -- Trim spaces / lines
  use ({ "cappyzawa/trim.nvim"})

	-- Automatically set up your configuration after cloning packer.nvim
	-- Put this at the end after all plugins
	if PACKER_BOOTSTRAP then
		require("packer").sync()
	end
end)
