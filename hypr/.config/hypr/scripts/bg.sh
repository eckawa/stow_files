#!/bin/sh

while true
do
    nbg=$(ls -d dots/backgrounds/* | shuf -n 1)
    killall swaybg
    swaybg -m fill -i "$nbg" &
    wal -n -i "$nbg"
    sleep 90
done
